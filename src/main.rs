use clap::{App, Arg, SubCommand};
use log::LevelFilter;
use swayipc_async::{Connection, Fallible, NodeType};
#[macro_use]
extern crate log;

#[tokio::main]
async fn main() -> Fallible<()> {
    let mut sway = Connection::new().await?;
    let tree = sway.get_tree().await?;

    let output_node = tree
        .find_focused_as_ref(|node| node.node_type == NodeType::Workspace && !node.focus.is_empty())
        .unwrap();
    let active_output_size = format!("{} {}", &output_node.rect.width, &output_node.rect.height);

    let args = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("Helper for sway that uses the sway-ipc to provide some extra functunality in an easy way.")
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Enables verbose output"),
        )
        .subcommand(SubCommand::with_name("resizefloatingfocusedrootcontainer")
            .about("Resizes the the focused floating root container of a container")
            .arg(
                Arg::with_name("size")
                    .short("s")
                    .default_value(&active_output_size)
                    .help("Size to resize the root container to, default is the size of the output the container is on"),
                )
            .arg(
                Arg::with_name("position")
                    .short("p")
                    .default_value("0 0")
                    .help("Position to position the root container to"),
                )
            )
        .get_matches();

    pretty_env_logger::formatted_builder()
        .filter(
            None,
            if args.is_present("verbose") {
                LevelFilter::Debug
            } else {
                LevelFilter::Info
            },
        )
        .init();

    debug!("args: {:#?}", args);

    if let Some(args) = args.subcommand_matches("resizefloatingfocusedrootcontainer") {
        let root_container = tree
            .find_focused_as_ref(|node| node.node_type == NodeType::FloatingCon)
            .expect("No focused container that is part of or itself a floating container found");

        // TODO Handle scale
        let commands = format!(
            "[con_id=\"{root_id}\"] resize set {}; [con_id=\"{root_id}\"] move position {};",
            args.value_of("size").unwrap(),
            args.value_of("position").unwrap(),
            root_id = root_container.id
        );
        debug!("commands: {}", commands);

        sway.run_command(commands)
            .await?
            .iter()
            .for_each(|result| *result.as_ref().unwrap());
    } else {
        println!("{}", args.usage.unwrap());
        panic!("Needs valid subcommand, see -h");
    }

    Ok(())
}
